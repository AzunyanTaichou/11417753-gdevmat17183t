﻿using Gdevmat1718_01.Models;
using Gdevmat1718_01.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Gdevmat1718_01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int North = 0;
        private const int South = 1;
        private const int East = 2;
        private const int West = 3;
        private const int Idle = 4;
        private const int NorthEast = 5;
        private const int NorthWest = 6;
        private const int SouthEast = 7;
        private const int SouthWest = 8;
       
        private Cube myFirstCube = new Cube();

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 2";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            myFirstCube.Render(gl);

            //gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, RandomNumberGenerator.GenerateInt(0, 3) + "");
            /*
           int outcome = RandomNumberGenerator.GenerateInt(0, 8);
           switch (outcome)
           {

               case Idle:
                   break;        

               case North:
                   myFirstCube.posY++;
                   break;

               case South:
                   myFirstCube.posY--;
                   break;

               case East:
                   myFirstCube.posX++;
                   break;

               case West:
                   myFirstCube.posX--;
                   break;

               case NorthEast:
                   myFirstCube.posX++;
                   myFirstCube.posY++;
                   break;

               case NorthWest:
                   myFirstCube.posX--;
                   myFirstCube.posY++;
                   break;

               case SouthEast:
                   myFirstCube.posX++;
                   myFirstCube.posY--;      
                   break;

               case SouthWest:
                   myFirstCube.posX--;
                   myFirstCube.posY--;  
                   break;
                   
        }*/
        myFirstCube.color = new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1));

            double outcomeWithDiffProbability = RandomNumberGenerator.GenerateDouble(0, 1);
        
            if (outcomeWithDiffProbability < 0.2)
            {
                myFirstCube.posY++;
            }
            else if (outcomeWithDiffProbability >= 0.2 && outcomeWithDiffProbability < 0.4)
            {
                myFirstCube.posY--;
            }
            else if (outcomeWithDiffProbability >= 0.4 && outcomeWithDiffProbability < 0.6)
            {
                myFirstCube.posX--;
            }
            else
            {
                myFirstCube.posX++;
            }
        }


        #region INITIALIZATION

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion


    }
}
