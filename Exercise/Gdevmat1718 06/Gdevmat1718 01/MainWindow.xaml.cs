﻿using Gdevmat1718_01.Models;
using Gdevmat1718_01.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Gdevmat1718_01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        private Cube myCube = new Cube();
        private Circle myCircle = new Circle();
        private Vector3 wind = new Vector3(0.01f, 0, 0);
        private Vector3 gravity = new Vector3(0, -0.01f, 0);
        private Vector3 helium = new Vector3(0, 0.05f, 0);
        private int time;


        private Vector3 nepnep = new Vector3(0.01f, 0.05f, 0);
        private Vector3 vectorchwan = new Vector3(5.0f, 0.0f, 0);
        private Vector3 darthBayder = new Vector3(0f, -1.0f, 0);
        private Vector3 kisame = new Vector3(0f, 1.0f, 0);
        private Vector3 pader = new Vector3(1.0f, 0f, 0);
        private int baydersMind;


        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 6";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -60.0f);

            /*
            baydersMind = RandomNumberGenerator.GenerateInt(0, 20);

            if (baydersMind == 3)
            {
                myCircle.ApplyForce(darthBayder);
            }
            */

            
            // Circle
            if (myCircle.Position.y >= 23 || myCircle.Position.y <= -23)
            {
                myCircle.Velocity.y *= -1.0f;
            }

            if (myCircle.Position.x >= 45 || myCircle.Position.x <= -45)
            {
                myCircle.Velocity.x *= -1.0f;
            }

            // Cube
            if (myCube.Position.y >= 23 || myCube.Position.y <= -23)
            {
                myCube.Velocity.y *= -1.0f;
            }    

            if (myCube.Position.x >= 45 || myCube.Position.x <= -45)
            {
                myCube.Velocity.x *= -1.0f;
            }


            myCube.Render(gl);
            myCircle.Mass = 20;
            myCube.ApplyForce(wind);
            myCube.ApplyForce(gravity);
           

            myCircle.Render(gl);
            myCircle.ApplyForce(wind);
            myCircle.ApplyForce(helium);

            if (time == 150)
            {
                myCube.Position = new Vector3(0,0,0);
                myCircle.Position = new Vector3 (0,0,0);
                myCube.Velocity = new Vector3(0, 0, 0);
                myCircle.Velocity = new Vector3(0, 0, 0);

                time = 0;
            }
            time++;

        }
     
        #region INITIALIZATION

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
