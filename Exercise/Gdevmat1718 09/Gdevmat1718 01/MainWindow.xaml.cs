﻿using Gdevmat1718_01.Models;
using Gdevmat1718_01.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Gdevmat1718_01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();
        private List<Circle> circles = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();

        
        public Cube massivePlanet = new Cube()
        {
            Mass = 5,
            Scale = new Vector3(0, 0, 0)
        };

        /*
        private Cube smallPlanet = new Cube()
        {
            Mass = 1,
            Position = new Vector3(15, 20, 0),

        };
        */
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "Day 10";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100);

             //massivePlanet.Render(gl);
            //smallPlanet.Render(gl);

            // smallPlanet.ApplyForce(massivePlanet.CalculateAttraction(smallPlanet));
            foreach (var c in cubes)
            {
                c.Render(gl); 
                foreach (var anyshit in cubes)
                {
                    c.ApplyForce(anyshit.CalculateAttraction(c));
                }
                foreach (var p in circles)
                {
                    c.ApplyForce(p.CalculateAttraction(c));
                    foreach (var anyname in circles)
                    {
                        p.ApplyForce(anyname.CalculateAttraction(p));
                    }
                    foreach (var anynamev2 in cubes)
                    {
                        p.ApplyForce(anynamev2.CalculateAttraction(p));
                    }
                    p.Render(gl);
                }
               


            }
        }
        #region INITIALIZATION

        public MainWindow()
        {
            InitializeComponent();
            for (int i = 2; i <= 10; i++)
            {
                cubes.Add(new Cube()
                {
                    Mass = 10,
                    red = RandomNumberGenerator.GenerateDouble(0, 1),
                    green = RandomNumberGenerator.GenerateDouble(0, 1),
                    blue = RandomNumberGenerator.GenerateDouble(0, 1),
                   // alpha = 100,



                    Position = new Vector3((float)RandomNumberGenerator.GenerateDouble(-50, 50), (float)RandomNumberGenerator.GenerateDouble(-50, 50), 0),
                });
            }

            for (int i = 2; i <= 10; i++)
            {
                circles.Add(new Circle()
                {
                    Mass = 1,
                    red = RandomNumberGenerator.GenerateDouble(0, 1),
                    green = RandomNumberGenerator.GenerateDouble(0, 1),
                    blue = RandomNumberGenerator.GenerateDouble(0, 1),
                    //alpha = 100d,



                    Position = new Vector3((float)RandomNumberGenerator.GenerateDouble(-50, 50), (float)RandomNumberGenerator.GenerateDouble(-50, 50), 0),
                });
            }
        }
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
