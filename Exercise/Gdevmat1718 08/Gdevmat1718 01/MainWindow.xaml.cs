﻿using Gdevmat1718_01.Models;
using Gdevmat1718_01.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Gdevmat1718_01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();
        private List<Circle> circles = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();
        private int time = 0;

        private Liquid MedyoMainitNaTubig = new Liquid(0, 0, 100, 50, 0.9f);

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "Day 10";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100);

            MedyoMainitNaTubig.Render(gl);
            

            foreach (var c in cubes)
            {
                c.ApplyGravity();
                c.Scale = new Vector3(c.Mass / 2, c.Mass / 2, c.Mass / 2);


                if (MedyoMainitNaTubig.Contains(c))
                {
                    var dragForce = MedyoMainitNaTubig.CalculateDragForce(c);
                    c.ApplyForce(dragForce);
                }

                if (c.Position.y <= -40)
                {
                    c.Position.y = -40;
                    c.Velocity.y *= -1;
                }

                c.Render(gl);
            }

            foreach (var p in circles)
            {

                p.ApplyGravity();
                p.radius = p.Mass / 2;


                if (MedyoMainitNaTubig.Contains(p))
                {
                    var dragForce = MedyoMainitNaTubig.CalculateDragForce(p);
                    p.ApplyForce(dragForce);
                }

                if (p.Position.y <= -40)
                {
                    p.Position.y = -40;
                    p.Velocity.y *= -1;
                }



                p.Render(gl);
            }


            time++;
        }
        #region INITIALIZATION

        public MainWindow()
        {
            InitializeComponent();
            for (int i = 2; i <= 10; i++)
            {
                cubes.Add(new Cube()
                {
                    Mass = i,
                    red = RandomNumberGenerator.GenerateDouble(0, 1),
                    green = RandomNumberGenerator.GenerateDouble(0, 1),
                    blue = RandomNumberGenerator.GenerateDouble(0, 1),
                    alpha = 0.6d,



                    Position = new Vector3((float)RandomNumberGenerator.GenerateGaussian(0, 30), 23, 0),
                });
            }

            for (int i = 2; i <= 10; i++)
            {
                circles.Add(new Circle()
                {
                    Mass = i,
                    red = RandomNumberGenerator.GenerateDouble(0, 1),
                    green = RandomNumberGenerator.GenerateDouble(0, 1),
                    blue = RandomNumberGenerator.GenerateDouble(0, 1),
                    alpha = 0.6d,



                    Position = new Vector3((float)RandomNumberGenerator.GenerateGaussian(0, 30), 23, 0),
                });
            }
        }
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
